import { ADD_RESTAURANT, EDIT_RESTAURANT, GET_RESTAURANT, GET_LIST_RESTAURANT, DELETE_RESTAURANT } from '../constants/appConstants'
const DEFAULT_STATE=[]

const addRestaurant = (state, payload) =>{
    const arr = state.map(item => item)                             
    arr.push(payload)
    return arr
}

const getRestaurant = (state, {index}) =>{
    return state.map((item, targetIndex) =>targetIndex === index ? item : {} )
}


const getRestaurantList = (state) =>{
    if(!!state.length > 0){
        return state
    }else{
        return []
    }
}

const editRestaurant = (state, { index, ...data}) =>{
    return state.map((item, targetIndex) =>  targetIndex === index ? data : item )
}

const deleteRestaurant = (state, {index}) =>{
    return state.filter((item, targetIndex) => targetIndex !== index)     
}

export default (state = DEFAULT_STATE, { type, payload }) => {
    switch (type) {
        case ADD_RESTAURANT:
            return addRestaurant(state, payload)
        case EDIT_RESTAURANT:
            return getRestaurant(state, payload)
        case GET_RESTAURANT:
            return getRestaurantList(state, payload)
        case GET_LIST_RESTAURANT:
            return editRestaurant(state, payload)
        case DELETE_RESTAURANT:
            return deleteRestaurant(state)
      default:
        return state;
    }
  };
  