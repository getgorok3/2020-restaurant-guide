import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import RestaurantReducer from "../reducers/restaurantReducer";

const reducers = history =>
  combineReducers({
    restaurants: RestaurantReducer,
    router: connectRouter(history)
  });

export default reducers;
