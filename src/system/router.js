import React, { Component } from "react";
import { Route, Switch } from "react-router";
import { ROUTE_TO_HOME } from '../constants/routContants'
import Main from '../components/main'

export default class Router extends Component {
    render() {
      return (
        <Switch>
          <Route exact path={ROUTE_TO_HOME} component={Main} />
        </Switch>
      );
    }
  }
  