
// import { images } from '../utilities/index'
const rootColor = {
    nero: '#1A1A1A', 
    blue: '#3895D3', 
    snow: '#FFFAFA',
    bottomLine: '#F1F3F6',
    white: '#FFFFFF',
    yellow: '#FFCC11'
}

export const styles = {

    // _________________________________ BUTTON _________________________________ //
    buttonMainContainer: {display: 'flex', maxWidth: '160px',margin: '0 auto'},
    buttonSubContainer: {
        width: '95%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 5
    },
    buttonText: {textAlign: 'center'},
    // _________________________________ HEADER _________________________________ //
    headerBackground: {
        borderBottomWidth: '0.9px',
        borderBottomColor:  rootColor.bottomLine,
       
    },
    headerBar:{
        position: 'fixed', width: '100%',backgroundColor: rootColor.white,zIndex: 1, 
    },
   
    headerText: {
        color: rootColor.nero
    },
    headerIcon: {
        color: rootColor.nero
    },
    // circle:{
    //     width: '125px',
    //     height: '100px',
    //     position: 'absolute',
    //     borderRadius: '50%',
    //     backgroundColor: rootColor.snow
    // },
    // _________________________________ CONTAINER _________________________________ //
    flexCenter:{ flex: 1, alignItems: 'center', justifyContent: 'center',width: '100%' },
    flexNormal: {
        display: 'flex', justifyContent: 'center', alignItems: 'center',
        width: '100%'
    },
    flexHeader: {
        display: 'flex', justifyContent: 'center', alignItems: 'center',
        width: '80%'
    },
    flexlogo: {
        display: 'flex', justifyContent: 'flex-start', alignItems: 'center', maxWidth: '20%',width: '100%'
    },
    flexUser: {
        display: 'flex', justifyContent: 'flex-end', alignItems: 'center', maxWidth: '20%',width: '100%'
    },


    contentBackground: { 
        backgroundColor: rootColor.white,
        height: '100vh'
    },
    contentOuter: { 
        padding: '20px 20px', marginTop: 64, height: '100vh'
    },
    noneBackGround: { 
        backgroundColor: 'none',
    },

    // __________________________________   TEXT   _________________________________ //
    mainText: {
        fontSize: 30
    },
    titleText: {
        fontSize: 18,
        marginLeft: '2%',
        color: rootColor.nero
    },
      // __________________________________   Image   _________________________________ //
    logo: { width: '60px', height: '50px'},
    // __________________________________   Animetion   _________________________________ //
    colorfulGradient: {
        backgroundImage: "linear-gradient(to right, #eecda3   , #ef629f)",
    },
    carousel: {
        textAlign: 'center',
        height: '160px',
        lineHeight: '160px',
        background:' #364d79',
        overflow: 'hidden'
    }
}