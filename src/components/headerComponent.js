import React,{useState} from 'react';
import {  Layout, Typography, Menu } from 'antd';
import { styles } from './styles'
import {images} from '../utilities/index'
import {ROUTE_TO_HOME, ROUTE_TO_FAVORITE, ROUTE_TO_RESTAURANTS} from '../constants/routContants'

const {Text} = Typography
const { Header } = Layout;

export default function  MyHeader() {
    const [menu, setMenu] = useState(ROUTE_TO_HOME)

    function handleTabChange (e){
        setMenu(e.key)
    }
  
    return (
             <Header style={{  display: 'flex', ...styles.headerBar}}>
                 <div style={{...styles.flexlogo}}>
                 <img src={images.logo} alt="logo" style={{...styles.logo}}></img>
                <Text ellipsis strong style={{...styles.titleText}}>Trust Your Gut</Text>
                 </div>
                 <div style={{...styles.flexHeader}}>
                    <Menu  mode="horizontal" defaultSelectedKeys={[menu]} onClick={handleTabChange} 
                     style={{ lineHeight: '64px' }}>
                        <Menu.Item key={ROUTE_TO_HOME}>Home</Menu.Item>
                        <Menu.Item key={ROUTE_TO_RESTAURANTS}>Restaurants</Menu.Item>
                        <Menu.Item key={ROUTE_TO_FAVORITE}>Favorite</Menu.Item>
                    </Menu>
               </div>
               <div style={{...styles.flexUser}}>

               </div>
             </Header>
    )
}