import React from 'react';
import { Button } from 'antd';
import { styles } from './styles'

export default function MyButton (text,func){
    return (
        <Button type="primary"  size={"large"} onClick={func}
             style={{ ...styles.buttonMainContainer,...styles.buttonSubContainer,...styles.buttonText }}>
            Dark 
        </Button>
    )
}