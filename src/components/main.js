import React from 'react';
import { Typography  } from 'antd';
import { styles } from './styles'

const { Text } = Typography
export default function  Main() {
    return (
        <div style={{...styles.contentBackground, ...styles.flexNormal}}>
          <Text ellipsis>Main page</Text>
        </div>
    )
}