import React from 'react';
// import {  useSelector } from "react-redux";
import { Layout  } from 'antd';
import MyHeader from './headerComponent'
import Route from '../system/router'
import { styles } from './styles';

const { Content, Footer } = Layout;
export default function  Home() {
    // const restaurants = useSelector(({restaurants}) => restaurants,[])
    // const dispatch = useDispatch()
  
    return (
        <Layout>
             <MyHeader />
             <Content className="site-layout" style={{ ...styles.contentOuter }}>
               <Route />
            </Content>
            <Footer style={{ textAlign: 'center' }}>Created by Thanakan Thanakwang</Footer>
        </Layout>
    )
}